/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

/**
 *
 * @author Rattanalak
 */
public abstract class Shape {
    private String ShapeName;

    public Shape(String ShapeName) {
        this.ShapeName = ShapeName;
    }

    public String getShapeName() {
        return ShapeName;
    }
    
    public abstract double calArea();
    public abstract double calPerimeter();
}

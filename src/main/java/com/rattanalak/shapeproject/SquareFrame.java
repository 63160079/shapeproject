/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Rattanalak
 */
public class SquareFrame extends JFrame{
    JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;
    
    public SquareFrame(){
        super("Square");
        this.setSize(350, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblSide = new JLabel("side: ", JLabel.TRAILING);
        lblSide.setSize(40, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);
        
        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Squre side = ??? area= ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.RED);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lblResult.setText("Square Side = " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });

    }
    public static void main(String[] args) {
        SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}

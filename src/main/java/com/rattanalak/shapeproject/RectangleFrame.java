/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Rattanalak
 */
public class RectangleFrame extends JFrame {

    JLabel lblWide;
    JLabel lblHeight;
    JTextField txtWide;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;
    

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(450, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWide = new JLabel("wide: ", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        this.add(lblWide);
        
        lblHeight = new JLabel("Height: ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtWide = new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        this.add(txtWide);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);
        this.add(txtHeight);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Rectangle wide=??? height=??? area=??? perimeter=???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(450, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.CYAN);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWide = txtWide.getText();
                    double wide = Double.parseDouble(strWide);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(wide,height);
                    lblResult.setText("Recttangle wide = " + String.format("%.2f", rectangle.getWide())
                            +" Height = " + String.format("%.2f", rectangle.getHeight())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
        });
    
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
